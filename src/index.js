// const express = require('express');
import express from 'express';
express.iamshinder = 'Shinder';
const url = require('url');

const fs = require('fs');
const upload = require(__dirname+'/upload');
const uuid = require('uuid');
const session = require('express-session');
const MysqlStore = require('express-mysql-session')(session);
const moment = require('moment-timezone');
const db = require(__dirname + '/db_connect2');
const getDB = require(__dirname + '/mdb_connect');
const cors = require('cors');
const axios = require('axios');
const cheerio = require('cheerio');

require('dotenv').config();

const app = express();
app.set('view engine', 'ejs'); // 設定樣版引擎
app.use(express.urlencoded({extended:false}));
app.use(express.json());

const sessionStore = new MysqlStore({}, db);
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'kjghdkfghdkf9854',
    store: sessionStore,
    cookie: {
        maxAge: 1200000
    }
}));

const whiteList = [
    'http://localhost:63342',
    'http://localhost:3000',
    'http://127.0.0.1:5500',
    'http://localhost:5500',
    undefined,
];

const corsOptions = {
    credentials: true,
    origin: function(origin, cb){
        // console.log('origin:', origin);
        if(whiteList.indexOf(origin) < 0){
            cb(null, false); // 不給過
        } else {
            cb(null, true);
        }
    }
};
app.use(cors(corsOptions));

app.use((req, res, next)=>{
    // 把 session 的資料放到 locals 裡, 用來傳給樣版 ejs
    if(req.session.loginUser){
        res.locals.loginUser = req.session.loginUser;
    } else {
        res.locals.loginUser = {};
    }

    next();
});

app.get('/', (req, res)=>{
    console.log(uuid.v4());
    res.render('home', {name: 'Shinder'});
});

app.get('/abc', (req, res)=>{
    res.send(`<h2>AABBBCCC</h2>`);
});

app.get('/sales-json', (req, res)=>{
    const sales = require(__dirname + '/../data/sales');
    //res.send(`<h2>${sales[0].name}</h2>`);
    res.render('sales-table', {sales:sales})
});

app.get('/try-qs', (req, res)=>{
    // http://localhost:3000/try-qs?a[age]=20&a[name]=bill
    // http://localhost:3000/try-qs?a[]=2&a[]=bill
    // http://localhost:3000/try-qs?a=2&a=bill
    res.json(req.query);

    /*
    const output = {
        url: req.url
    };
    output.urlParts = url.parse(req.url, true);
    // output.urlParts = url.parse("http://localhost:3000/try-qs?a=2&b=bill", true);
    //res.json(output);
    res.json(output.urlParts);

     */
});

app.get('/try-post', (req, res)=>{
    res.render('try-post-form');
});
app.post('/try-post', (req, res)=>{
    res.json(req.body);
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    const output = {
        body: req.body,
        file: req.file,
    };
    console.log(req.file);
    if(req.file && req.file.originalname){
        let ext = ''; //副檔名
        switch(req.file.mimetype){
            case 'image/jpeg':
                ext = '.jpg';
                break;
            case 'image/png':
                ext = '.png';
                break;
            case 'image/gif':
                ext = '.gif';
                break;
        }
        if(ext){
            let filename = uuid.v4() + ext;
            output.newName = filename;
            fs.rename(
                req.file.path,
                './public/img/' + filename,
                error=>{}
            );
        } else {
            fs.unlink(req.file.path, error=>{});
        }
    }

    res.json(output);
});

app.get('/try-params1/:action?/:id?', (req,res)=>{
   res.json(req.params);
});
app.get(/^\/mobile\/09\d{2}-?\d{3}-?\d{3}$/, (req,res)=>{
    let m = req.url.slice(8);
    m = m.split('?')[0];
    m = m.split('-').join('');
    res.json({
        url: m
    });
});

require(__dirname + '/admins/admin1')(app);
app.use(   require(__dirname + '/admins/admin2')  );
app.use('/admin3',  require(__dirname + '/admins/admin3')  );

app.get('/try-session', (req, res)=>{
    req.session.my_var = req.session.my_var || 0;
    req.session.my_var++;
    res.json({
        my_var : req.session.my_var,
        session: req.session
    });
});
import memberRouter from './routes/member';
app.use('/member', memberRouter);
app.get('/sess', (req, res)=>{
   res.json(req.session);
});

app.get('/try-moment', (req, res)=>{
    const fm = "YYYY-MM-DD HH:mm:ss";
    const m1 = moment(req.session.cookie.expires);
    const m2 = moment(new Date());
    const m3 = moment('2020-02-18');

    res.json({
        'local-m1': m1.format(fm),
        'local-m2': m2.format(fm),
        'local-m3': m3.format(fm),
        'london-m1': m1.tz('Europe/London').format(fm),
        'london-m2': m2.tz('Europe/London').format(fm),
        'london-m3': m3.tz('Europe/London').format(fm),
    });
});

app.get('/try-db', (req, res)=>{
    req.session.hello = 'shinder';
    const sql = "SELECT * FROM address_book LIMIT 3";
    db.query(sql).then(([results, fields])=>{
        res.json(results);
    });
});

app.get('/try-mdb', (req, res)=>{
    const mdb = getDB();
    mdb.collection('books')
        .find({})
        .toArray()  // Cursor 的 toArray()
        .then((ar)=>{
            res.json(ar);
        })
});
app.get('/try-mdb2', (req, res)=>{
    const mdb = getDB();
    const ar = [];
    mdb.collection('books')
        .aggregate([
            {
                $lookup: {
                    from: 'publishers',
                    foreignField: '_id',
                    localField: 'publisher_id',
                    as: 'publisher'
                }
            }
        ])
        .forEach(function(doc){
            ar.push(doc)
        }).then(()=>{
            res.json(ar);
        })
});

// app.use('/address-book', require(__dirname +'/routes/address_book'));
import addressBookRouter from './routes/address_book2';
app.use('/address-book', addressBookRouter);

import addressBookMongoRouter from './routes/address_book_mongo';
app.use('/address-book-mongo', addressBookMongoRouter);

app.get('/yahoo', (req, res)=>{
    axios.get('https://tw.yahoo.com/')
    // axios.get('https://www.104.com.tw/job/6gaji?jobsource=hotjob_chr')
        .then(response=>{
            //res.send(response.data);
            const $ = cheerio.load(response.data);
            $('img').each(function(i, el){
                // console.log(el.attribs.src);
                res.write(el.attribs.src + '\n');
            });
            res.end('');
        });
});

app.use('/jwt-try', require(__dirname + '/routes/jwt-try'));

app.use(express.static('public'));

// 放在所有路由的後面
app.use((req, res)=>{
    res.type('text/html');
    res.status = 404;
    res.send(`
    path: ${req.url} <br>
    <h2>404 - page not found</h2>
    `);
});
app.listen(3000, ()=>{
    // console.log('global:', global);
    // console.log('process.env:', process.env);
    console.log('process.argv:', process.argv);
    console.log('server started!');
});