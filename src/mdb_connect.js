const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'test';
let _db;  // 存放對應 DB 的物件

const client = new MongoClient(url, {useUnifiedTopology: true});

client.connect()
    .then(c => {
        _db = client.db(dbName);  // c 同 client
    })
    .catch(error=>{
        console.log('Cannot connect the mongodb server!');
        console.log(error);
    });

const getDB = ()=>{
    if(!_db) throw new Error('No MongoDB connection!');
    return _db;
};
module.exports =  getDB;