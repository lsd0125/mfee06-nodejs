// 1. 引入 express
var express = require('express');

// 2. 建立 web server 物件
var app = express();

// 3. 路由
app.get('/', function (req, res) {
    res.send('Hello World!');
});

// *** 此段放在所有路由設定的後面 ***
app.use((req, res) => {
    res.type('text/plain');
    res.status(404);
    res.send('404 - 找不到網頁');
});


// 4. Server 偵聽
app.listen(3000, function () {
    console.log('啟動 server 偵聽埠號 3000');
});
