// src/person_test.js

const Person = require('./person');
// const { Person, f } = require('./Person');

const p1 = new Person('Bill', 26);
const p2 = new Person;

console.log(p1.toJSON());
// console.log(p1.toString());
console.log(p2.toJSON());
// console.log(p2.toString());
// console.log(f(7));
