// src/person.js
class Person {
    constructor(name='noname', age=20) {
        this.name = name;
        this.age = age;
    }
    toJSON(){
        const obj = {
            name: this.name,
            age: this.age,
        };
        return JSON.stringify(obj);
    }
}

const f = a=>a*a;

module.exports = Person; // node 匯出類別
// module.exports = {f, Person};