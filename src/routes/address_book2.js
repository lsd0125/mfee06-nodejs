/*
baseUrl:  /address-book

CRUD:
    Create (insert)
        get:  /add
        post: /add (...)

    Update
        get:  /edit/:sid
        post: /edit/:sid (...)

    Delete
        post: /delete/:sid

    Read
        get: /:page/:category?
 */
import express from 'express';
import moment from 'moment-timezone';
import upload from '../upload';
import db from '../db_connect2';

const router = express.Router();

router.use((req, res, next)=>{
    res.locals.title = '通訊錄';
    next();
});

router.get('/delete/:sid?', (req, res)=>{
    const sql = "DELETE FROM `address_book` WHERE sid=?";
    db.query(sql, [req.params.sid])
        .then(([results, _])=>{
            // res.redirect('/address-book');
            if(req.get('Referer')){
                // 如果有[從哪裡來]的資料
                res.redirect( req.get('Referer') );
            } else {
                res.redirect('/address-book');
            }
        })
        .catch(ex=>{
            console.log('ex:', ex);
            res.json({
                success: false,
                info: '無法刪除資料'
            });
        })
});

router.get('/add',(req, res)=>{
    res.render('address-book/add');
});

// upload.none() 用來解析 multipart/form-data 格式的 middleware
router.post('/add', upload.none(), (req, res)=>{
    const output = {
        success: false,
        error: '',
    };
    // TODO: 應該檢查表單進來的資料
    if(req.body.name.length<2){
        output.error = '姓名字元長度太短';
        return res.json(output);
    }

    const email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(!email_pattern.test(req.body.email)){
        output.error = 'Email 格式錯誤';
        return res.json(output);
    }
    req.body.created_at = new Date();
    const sql = "INSERT INTO `address_book` SET ?";

    db.query(sql, [req.body])
        .then(([results, _])=>{
            output.results = results;
            if(results.affectedRows===1){
                output.success = true;
            }
            res.json(output);
        })
        .catch(ex=>{
            console.log('ex:', ex);
        })
});

router.get('/edit/:sid',(req, res)=>{
    const sql = "SELECT * FROM address_book WHERE sid=?";
    db.query(sql, [req.params.sid])
        .then(([results, fields])=>{
            if(results.length){
                results[0].birthday = moment(results[0].birthday).format('YYYY-MM-DD');
                // res.render('address-book/edit', results[0]);
                res.render('address-book/edit', {...results[0], myRow: results[0] });
            } else {
                res.redirect('/address-book');
            }
        })
        .catch(ex=>{
            console.log('ex:', ex);
        })
});

router.post('/edit', upload.none(), (req, res)=>{
    const output = {
        success: false,
        error: '',
    };
    // TODO: 應該檢查表單進來的資料
    if(req.body.name.length<2){
        output.error = '姓名字元長度太短';
        return res.json(output);
    }

    const email_pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(!email_pattern.test(req.body.email)){
        output.error = 'Email 格式錯誤';
        return res.json(output);
    }

    const data = {...req.body}; // 第一層複製
    delete data.sid; // 移除 sid

    const sql = "UPDATE `address_book` SET ? WHERE sid=?";

    db.query(sql, [data, req.body.sid])
        .then(([results, _])=>{
            console.log('update-----------------');
            console.log(results);
            output.results = results;
            if(results.changedRows===1){
                output.success = true;
            } else {
                output.error = '資料沒有變更';
            }
            res.json(output);
        })
        .catch(ex=>{
            console.log('ex:', ex);
        })
});

const getDataByPage = (req)=>{
    const perPage = 3;

    return new Promise((resolve, reject)=>{
        // if(!req.session.loginUser){
        //     resolve({
        //         success: false,
        //         info: '請登入會員'
        //     });
        //     return;
        // }

        let page = parseInt(req.params.page) || 1;
        const output = {
            totalRows: 0, // 總筆數
            perPage: perPage, // 每一頁最多幾筆
            totalPages: 0, //總頁數
            page: page, // 用戶要查看的頁數
            rows: 0, // 當頁的資料
        };

        const t_sql = "SELECT COUNT(1) num FROM address_book";
        db.query(t_sql)
            .then(([results, fields])=>{
                output.totalRows = results[0].num;
                output.totalPages = Math.ceil(output.totalRows/perPage);
                if(output.page < 1) output.page=1;
                if(output.page > output.totalPages) output.page=output.totalPages;
                const sql = `SELECT * FROM address_book ORDER BY sid DESC LIMIT ${(output.page-1)*output.perPage}, ${output.perPage}`;
                return db.query(sql);
            })
            .then(([results, fields])=>{
                const fm = 'YYYY-MM-DD';
                for(let i of results){
                    i.birthday = moment(i.birthday).format(fm);
                }
                output.rows = results;
                output.user = req.session.loginUser || {};
                output.success = true;
                resolve(output);
            })
            .catch(ex=>{
                reject(ex);
            });
    });

};

router.get('/list/:page?', async (req, res)=>{
    const output = await getDataByPage(req);
    res.json(output);
});

router.get('/:page?', async (req, res)=>{
    const output = await getDataByPage(req);
    res.render('address-book/list', output);
});

export default router;