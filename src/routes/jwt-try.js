const express = require('express');
const jwt = require('jsonwebtoken');

const router = express.Router();


const users = {
    'shinder': {
        nickname: '小明',
        pw: '23456'
    },
    'hello': {
        nickname: '是在哈囉',
        pw: '123456'
    },
};

router.get('/users', (req, res)=>{
    res.json(users);
});

router.post('/login', (req, res)=>{
    const output = {
        success: false,
        error: '帳號或密碼錯誤',
        body: req.body,
    };

    if(req.body.account && users[req.body.account] ){
        // 帳號是對的
        if(req.body.password===users[req.body.account].pw){
            // 密碼也是對的
            const payload = {
                id: req.body.account,
                nickname: users[req.body.account].nickname,
            }

            output.token = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET);
            output.success = true;
            delete output.error;
        }
    }

    res.json(output);
});

router.get('/check', (req, res)=>{
    let noVerify = true;
    const authHeader = req.get('Authorization');
    if(!!authHeader){
        const token = authHeader.split(' ')[1];
        if(!!token){
            noVerify = false;
            jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload)=>{
                if(err){
                    res.json({ error: err });
                } else {
                    res.json(payload);
                }

            });

        }
    }
    if(noVerify){
        res.json({msg: 'no token'});
    }
});

router.get('/logout', (req, res)=>{
    delete req.session.loginUser; // 清掉 session 變數
    res.redirect('/'); // 轉向到別的頁面
});

module.exports = router;